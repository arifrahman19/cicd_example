FROM node:11-alpine

# ARG PORT

RUN mkdir -p /usr/src/cicd_example

WORKDIR /usr/src/cicd_example

COPY package*.json ./
RUN npm config set package-lock false
RUN npm install
RUN npm audit fix
# RUN echo "module.exports = { PORT: $PORT }" > config.js
COPY . .


EXPOSE 8080

CMD ["npm", "start"]